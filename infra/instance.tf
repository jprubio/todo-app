resource "aws_instance" "web" {
  ami           = var.ami["us-east-1"]
  instance_type = var.tipo_instancia
  key_name      = "chave_ssh"
  subnet_id     = aws_subnet.subnet-publica-todo-app-jp.id
  vpc_security_group_ids = [
    aws_security_group.http-todo-app-jp.id,
    aws_security_group.out-all-todo-app-jp.id,
    aws_security_group.ssh-todo-app-jp.id
  ]
  tags = {
    Name = "SRV-Web"
    Terraform = "True"
  }
}

resource "aws_instance" "db" {
  ami           = var.ami["us-east-1"]
  instance_type = var.tipo_instancia
  key_name      = "chave_ssh"
  subnet_id     = aws_subnet.subnet-privada-todo-app-jp.id
  vpc_security_group_ids = [
    aws_security_group.pass-db.id,
    aws_security_group.out-all-todo-app-jp.id,
    aws_security_group.ssh-in-local.id
  ]
  tags = {
    Name = "SRV-Database"
    Terraform = "True"
  }
}

