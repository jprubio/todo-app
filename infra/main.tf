
resource "aws_vpc" "vpc-todo-app-jp" {
  cidr_block             = "10.0.0.0/16"
  enable_dns_support     = "true"
  enable_dns_hostnames   = "true"
  enable_classiclink     = "false"

  tags = {
    Name = "VPC-todo-app-jp"
    Terraform = "True"
  }
}

resource "aws_main_route_table_association" "correction" {
  vpc_id         = aws_vpc.vpc-todo-app-jp.id
  route_table_id = aws_route_table.rtb-todo-app-jp.id
}

resource "aws_subnet" "subnet-publica-todo-app-jp" {
  vpc_id                  = aws_vpc.vpc-todo-app-jp.id
  cidr_block              = "10.0.1.0/24"
  map_public_ip_on_launch = "true"  
  availability_zone       = "us-east-1a"

  tags = {
    Name      = "Sub-Publica"
    Terraform = "True"
  }
}

resource "aws_subnet" "subnet-privada-todo-app-jp" {
  vpc_id                  = aws_vpc.vpc-todo-app-jp.id
  cidr_block              = "10.0.10.0/24"
  map_public_ip_on_launch = "true" 
  availability_zone       = "us-east-1a"

  tags = {
    Name      = "Sub-Privada"
    Terraform = "True"
  }
}

resource "aws_default_network_acl" "net-acl-todo-app-jp" {
  default_network_acl_id = aws_vpc.vpc-todo-app-jp.default_network_acl_id

  ingress {
    protocol   = -1
    rule_no    = 100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
  }

  egress {
    protocol   = -1
    rule_no    = 100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
  }

  tags = {
    Name      = "Net-ACL-todo-app-jp"
    Terraform = "True"
  }
}

resource "aws_route_table" "rt-todo-app-jp" {
  vpc_id = aws_vpc.vpc-todo-app-jp.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gate-todo-app-jp.id
  }

  tags = {
    Name = "router-tab-todo-app-jp"
    Terraform = "True"
  }
}

resource "aws_internet_gateway" "gate-todo-app-jp" {
  vpc_id = aws_vpc.vpc-todo-app-jp.id

  tags = {
    Name = "gate-todo-app-jp"
    Terraform = "True"
  }
}
