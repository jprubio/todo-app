resource "aws_security_group" "ssh-todo-app-jp" {
  name          = "ssh-todo-app-jp"
  vpc_id        = aws_vpc.vpc-todo-app-jp.id
  ingress {
    description = "SSH admin"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["177.42.157.155/32"]
  }
  tags = {
    Name        = "ssh-todo-app-jp"
    Terraform   = "true"
  }
}

resource "aws_security_group" "ssh-local" {
  name          = "ssh-local"
  vpc_id        = aws_vpc.vpc-todo-app-jp.id
  ingress {
    description = "SSH admin"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name        = "ssh-local"
    Terraform   = "true"
  }
}

resource "aws_security_group" "out-all-todo-app-jp" {
  name          = "out-all-todo-app-jp"
  vpc_id        = aws_vpc.vpc-todo-app-jp.id
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name        = "out-all-todo-app-jp"
    Terraform   = "true"
  }
}

resource "aws_security_group" "db-todo-app-jp" {
  name          = "db-todo-app-jp"
  vpc_id        = aws_vpc.vpc-todo-app-jp.id
  ingress {
    from_port   = 27017
    to_port     = 27017
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    security_groups = [aws_security_group.http-todo-app-jp.id]
  }
  tags = {
    Name        = "db-todo-app-jp"
    Terraform   = "true"
  }
}

resource "aws_security_group" "http-todo-app-jp" {
  name          = "http-todo-app-jp"
  vpc_id        = aws_vpc.vpc-todo-app-jp.id
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name        = "http-todo-app-jp"
    Terraform   = "true"
  }
}
