variable "tipo_instancia" {
    type = string
    description = "Tipo de instância EC2 do servidor"
    default = "t2.micro"
}
variable "ami" {
    type = string
    description = "ID da AMI a ser utilizada"
}
variable "chave_ssh" {
    type = string
    description = "Chave SSH utilizada para acesso remoto"
    default = "aula-terraform"
}
